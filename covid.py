#!/usr/bin/env python3
# coding: utf-8

# # お題：新型コロナウイルス（COVID19）の感染者数の可視化に挑戦！
#
# ## 目的
#
# - 職場の同僚が ``Python``やりたい、と言っていたので、タイムリーなデータを教材にした
# - ゼロからプログラムを書けるようになるのではなく「どんな分析ができるのか」を知ること
#
# ## データの列の意味
#
# - このPDFと照らし合わせる
#   - https://www.stopcovid19.jp/data/covid19japan/000633827.pdf
# - ``date`` : 日付
# - ``name`` :  都道府県名（英語）
# - ``name_jp`` : 都道府県名（日本語）
# - ``ninspections`` : PCR検査実施人数
# - ``npatients`` : PCR検査陽性者
# - ``ncurrentpatients`` : 入院治療などを要する者
# - ``nheavycurrentpatients`` : 入院治療などを要する者のうち重症
# - ``nexits`` : 退院または療養解除となった者の数
# - ``ndeaths`` : 死亡（累積）
# - ``nunknowns`` : 確認中


from pathlib import Path

import altair as alt
import pandas as pd
from deprecated import deprecated
from icecream import ic

from covid19 import canvas, config, data, downloader, loader, log, version

__config = config.Config()
__logger = log.setup(__name__, __config.logfile)
__logger.debug("-" * 50)
__logger.debug(f"{__name__}")
__version__ = version.get_version()
ic(__version__)


def update(start: str = "2020-03-18") -> pd.DataFrame:
    data = downloader.update(start)
    data = preprocess(data)
    fname = __config.downloaded
    data.to_csv(fname, index=False)
    __logger.info(f"Saved : {fname}")
    period = show_period(data)
    print(f"Fetched period : {period}")
    return data


def preprocess(src: pd.DataFrame) -> pd.DataFrame:
    rdata = data.process(src)
    return rdata


def get_period(data: pd.DataFrame):
    """
    取得したデータの日付の範囲を計算する

    Parameters
    ----------
    data : pandas.DataFrame
        データ

    Returns
    -------
    tuple
        開始日／最終日のtupe
    """
    dates = pd.to_datetime(data["date"], unit="D").unique()
    start = pd.to_datetime(dates[0], unit="s")
    end = pd.to_datetime(dates[-1], unit="s")
    return (start, end)


def show_period(data: pd.DataFrame):
    """
    取得したデータの日付の範囲を表示する

    Parameters
    ----------
    data : pandas.DataFrame
        データ

    Returns
    -------
    str
        日付が入った文字列
    """
    s, e = get_period(data)
    start = f"{s.year}-{s.month:02}-{s.day:02}"
    end = f"{e.year}-{e.month:02}-{e.day:02}"
    period = f"From {start} to {end}"
    return period


def summary(data, name):
    """
    サマリー図を作成

    Parameters
    ----------
    data : pandas.DataFrame
        データフレーム
    name : str
        都道府県の名前（ローマ字）

    Returns
    -------
    altair.Chart
        グラフ
    """

    __logger.info(name)
    c0 = canvas.npatients(data, name).properties(width=2000)
    c1 = daily_summary(data, name).properties(width=2000)
    c2 = canvas.rpositive(data, name).properties(width=2000)
    chart = c0 & c1 & c2
    return chart


def daily_summary(data, name):
    c1 = canvas.npositive(data, name)
    c2 = canvas.ndeath(data, name)
    c3 = canvas.nheavy(data, name)
    c4 = canvas.npositive7d(data, name)
    chart = c1 + c2 + c3 + c4
    return chart


def get_list(data):
    """
    患者数の多い順に都道府県を並べる

    Parameters
    ----------
    data : pandas.DataFrame
        日付ごとの stopcovid19 のデータ

    Returns
    -------
    pandas.DataFrame
        合計患者数の多い順に並び替えたデータフレーム
    """
    g = ["name_jp", "name"]
    c = ["npositive"]
    return (
        data.groupby(g)[c]
        .sum()
        .reset_index()
        .sort_values(by="npositive", ascending=False)
    )


if __name__ == "__main__":

    import argparse
    import sys

    description = "COVID19の感染者のグラフを作ってみよう！"
    ap = argparse.ArgumentParser(description=description)

    gr = ap.add_mutually_exclusive_group()
    gr.add_argument("-n", type=int, help="患者数の多い都道府県を順番に表示")
    gr.add_argument(
        "-u", "--update", action="store_true", help=f"Update {__config.downloaded}"
    )
    gr.add_argument("-p", "--prefecture", nargs="+", help="Set 都道府県s（in English）")

    args = ic(ap.parse_args())

    if args.update:
        update()
        sys.exit()

    if args.n:
        data = loader.load(__config.downloaded)
        lt = get_list(data)
        print(lt.head(args.n))
        sys.exit()

    if args.prefecture:
        data = loader.load(__config.downloaded)

        prefs = args.prefecture

        for pref in prefs:
            q = f'name == "{pref}"'
            qdata = data.query(q)

            chart = summary(qdata, pref)
            fname = Path(__config.images) / f"{pref}.png"
            chart.save(str(fname))
            __logger.info(f"Saved as {fname}")
