# 説明／メモ

- データに関する補足説明などを書き加えていく

## 感染者数のデータを探す

- ``COVID19``の感染者数のデータは stopcovid19 のウェブサイトに``csv``形式で、オープンデータとして公開されていた
  - https://www.stopcovid19.jp/
- 集約されたデータはここにある
  - https://www.stopcovid19.jp/data/covid19japan.csv
  - https://www.stopcovid19.jp/data/covid19japan-all.csv
  - 上の違いはきちんと調べていない
- 日ごとのデータはページからダウンロードできる
  - https://www.stopcovid19.jp/tableview.html
  - ページ下部にあるドロップダウンで日付を選択し、その横にある　``CSVダウンロード：各都道府県...`` の下線部をクリックするとCSVファイルがダウンロードできる
- このCSVファイルは、以下のフォーマットのURLを直接叩くことでもダウンロードできる
  - 2020-04-27 : https://www.stopcovid19.jp/data/covid19japan/2020-04-27.csv
  - 2020-05-22 : https://www.stopcovid19.jp/data/covid19japan/2020-05-22.csv
- このフォーマットをよく見ると、日付部分を変更すれば、直接アクセスできることがわかる
  - わざわざブラウザからダウンロードして保存する必要がない
  - 今回はこの方法で進める


## データの列の意味

- このPDFと照らし合わせる
  - https://www.stopcovid19.jp/data/covid19japan/000633827.pdf
- ``date`` : 日付
- ``name`` :  都道府県名（英語）
- ``name_jp`` : 都道府県名（日本語）
- ``ninspections`` : PCR検査実施人数
- ``npatients`` : PCR検査陽性者
- ``ncurrentpatients`` : 入院治療などを要する者
- ``nheavycurrentpatients`` : 入院治療などを要する者のうち重症
- ``nexits`` : 退院または療養解除となった者の数
- ``ndeaths`` : 死亡（累積）
- ``nunknowns`` : 確認中


## 解析に使う ``Python`` パッケージを紹介

- このデータを ``Python``（特に ``Pandas`` と ``Altair`` ）を使って可視化する
- [Pandas](https://pandas.pydata.org)`` : データ分析するためのフレームワーク
- [Altair](https://altair-viz.github.io) : データ可視化するためのフレームワーク
