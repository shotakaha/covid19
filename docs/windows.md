# Windows PC で実行するには

## Python をインストールする

- [Python公式サイトのダウンロードページ](https://www.python.org/downloads/) からダウンロードする
  - 最新版（v3.8.3）をダウンロードした
  - https://www.python.org/downloads/release/python-383/ の ``Files`` にある ``Windows x86-64 web-based installer`` を選択した
  - そのままインストーラが走りはじめた
  - 最初のページで ``[Add to PATH ...]`` にチェックを入れて、その後は素直に``[OK]``を選択し、残りはそのまま従った

## ``pip`` を更新する

- ``pip`` は ``Python`` のパッケージをインストール／アンインストールするのに使うモジュール
  - ``Pip Install Python`` ／ ``Pip Install Package`` の再帰的な略語だそう
- 上記の方法でインストールしたときについてきた ``pip`` のバージョンが古かったので更新した
  - ついでに ``setuptools`` も更新した
  - ついでに ``wheel`` をインストールした
- コマンドプロンプトを起動し、以下を入力した

```bash
$ py -m pip install --upgrade pip
$ py -m pip install --upgrade setuptools
$ py -m pip install wheel
```

## 必要なライブラリを追加する

- ``altair`` をインストールする
- ``pandas`` をインストールする
- ``streamlit`` をインストールする

```bash
$ py -m pip install altair
$ py -m pip install pandas
$ py -m pip install streamlit
```

## このリポジトリに移動する

- リポジトリにあるファイルをダウンロードしておく
- コマンドプロンプトを起動し、上記のフォルダに移動する

```bash
$ cd パッケージまでのパス
$ dir    ## ファイルの中身を確認

## 1日の初めて1回だけ実行する
$ py covid19.py -u

$ py covid19.py -p Ibaraki
## html/Ibaraki.html のファイルをブラウザで確認する
$ py covid19.py -p Tokyo
## html/Tokyo.html のファイルをブラウザで確認する
```


---

## 補足：Windowsのコマンドプロンプトを起動する方法

- ``[スタート]``横の検索ボックスで ``cmd`` を入力し``cmd.exe`` を起動する
- ``cd ディレクトリ名``・・・ディレクトリを移動（``change directory``）
- ``dir``・・・ディレクトリの中身を表示（``Linux``だと``ls``)
- ``py スクリプト.py （オプション） 引数``・・・``Python``スクリプトを実行
