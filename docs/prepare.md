# 事前準備

以下のツールが使えるパソコン環境を用意しておく

- ``Python``が動く環境
- ``Git``が動く環境

実はこの部分が一番大変かも・・・具体的な設定方法は、とりあえずウェブを検索


## ``Python`` ライブラリインストール

- ``altair`` をインストールする
- ``pandas`` をインストールする
- ``streamlit`` をインストールする

```bash
$ pip install altair
$ pip install pandas
$ pip install streamlit
```

### ``poetry`` を使ってライブラリをインストール

- 上記の ``pip`` よりモダン（？）な方法
- まず ``poetry`` をインストールする
  - https://python-poetry.org/
  - ``curl``を使った方法でインストールする
- クローンしたリポジトリで ``poetry.lock`` のあるディレクトリに移動

```bash
$ poetry install  ## poetry.lock に書いてる内容でインストール
$ poetry update   ## outdated なパッケージを up-to-date に更新
```

## 自分のパソコンにリポジトリをクローンする

- ``git`` コマンドが必要

```bash
$ git clone https://gitlab.com/shotakaha/covid19.git
Cloning into 'covid19'...
remote: Enumerating objects: 52, done.
remote: Counting objects: 100% (52/52), done.
remote: Compressing objects: 100% (47/47), done.
remote: Total 52 (delta 22), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (52/52), 100.97 KiB | 3.06 MiB/s, done.
Resolving deltas: 100% (22/22), done.
```

## ソースコード一式をダウンロードする

- 上記の``git``コマンドが使えない場合は、``GitLab``から直接ソースコードをダウンロードする
- スクリプトを改善するたびに ``リリースタグ`` をつけるようにするので、以下のページから最新版をダウンロードする
- https://gitlab.com/shotakaha/covid19/-/releases
- ``zip``でダウンロードして、各自で展開して使う

---

## リポジトリのディレクトリ構成

```
.
├── README.md
├── covid19.ipynb    ## Jupyter Notebook
├── covid19.py       ## Python script
├── data             
│   └── covid19.csv  ## 感染者データをまとめたファイル（``-u``オプションを付けて実行すると作成される）
└── html
    └── test1.html   ## 作成したプロットをHTML形式で保存するためのディレクトリ
```

- ``data/`` に保存したファイルはリポジトリに追加できないようにしている
- ``html/`` に保存したファイルはリポジトリに追加できないようにしている

---
