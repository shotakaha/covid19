import datetime

import streamlit as st

import covid as covid19
from covid19 import config, data, downloader, loader, log

__config = config.Config()


def update():
    return covid19.update()


@st.cache
def load():
    data = loader.load(__config.downloaded)
    return data


@st.cache
def get_list(data):
    return covid19.get_list(data)


def show_list():
    st.markdown("## 検査陽性数の合計が多い都道府県を表示")
    n = st.number_input("表示数を選択", 1, 47, 5)

    data = load()
    _sorted = get_list(data)
    st.write(_sorted.head(n))
    return


def select_pref(data, key, index=12):
    _sorted = get_list(data)
    _pref = st.selectbox(
        "都道府県を選択",
        list(_sorted["name"]),
        index=index,
        key=key,
    )
    return _pref


def show_summary():
    st.markdown("## 都道府県別の検査陽性数")
    st.markdown(
        """
        - 検査陽性数（累積）と検査陽性数（新規）の推移を表示（橙色）
        - 検査陽性数（新規）がマイナスの日は赤色で表示（例：Aichiなど）
        - 検査陽性数（新規）には直前7日間の移動平均と標準偏差の推移も重ねて表示（緑色）
        - 死亡数（新規）の推移も表示（灰色）
        - デフォルト：茨城県
        """
    )

    data = load()
    _pref = select_pref(data, key="summary")
    ##print(f'Show {_pref}')
    q = f'name == "{_pref}"'
    qdata = data.query(q)
    chart = covid19.summary(qdata, name=_pref)
    chart


def show_copyright():
    v = covid19.__version__
    st.markdown("---")
    st.markdown(
        f"""
        &copy; 2020 shotakaha
        - v{v}
        - GitLab Repos : https://gitlab.com/shotakaha/covid19
        - Data Source : https://www.stopcovid19.jp/
    """
    )
    return


def get_update():
    u = st.button("データを更新")
    if u:
        with st.spinner("データを更新中..."):
            update()
        st.success("更新完了")


def show_dates():
    data = load()
    dates = covid19.show_period(data)
    st.markdown(dates)
    get_update()


def show_compare():
    st.markdown("## 比較したい都道府県")
    st.markdown(
        """
        - 他の都道府県と比較したいときのために追加
        - 縦軸に注意して比較する（スケールを合わせる機能はない）
        - デフォルト：東京都
        """
    )

    data = load()
    _pref = select_pref(data, key="compare", index=0)
    q = f'name == "{_pref}"'
    qdata = data.query(q)
    chart = covid19.summary(qdata, name=_pref)
    chart
    ##print(f'Show {_pref}')

    chart.properties(height=500)


def container():
    st.markdown("# COVID19の検査陽性数を確認しよう！")

    show_dates()
    show_list()
    show_summary()
    show_compare()
    show_copyright()
    return


container()
