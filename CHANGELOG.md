## 0.2.1 (2021-03-17)

### Fix

- **covid.py**: fixed -p option to take multiple values (nargs+)
- **covid.py**: fixed option -n error
- **canvas.rpositive**: Y軸の値を(0, 1)に制限した

## 0.2.0 (2021-01-30)

### Feat

- update version

### Fix

- **deprecated**: bar
- **deprecated**: show_ndeath, show_nheavy, show_rpositive, show_npositive7d
- **deprecated**: show_npatients, show_npositive
- **deprecated**: point
- **deprecated**: circle

## 0.0.16 (2021-01-30)

### Fix

- **covid.py**: deprecatedのマークを追加した
- **canvas.py**: グラフ生成を移動した
- **canvas.py**: 棒グラフを移行 : npositive, ndeath, nheavy
- **canvas.py**: グラフの説明を追加
- **canvas.py**: グラフを作成するモジュールを作成した
- **covid19.py**: fix -p

### Refactor

- **html**: remove html

## 0.0.15 (2021-01-30)

### Fix

- **deprecated**: もう使わない関数を削除した

## 0.0.14 (2021-01-30)

### Fix

- **data.py**: 7日の移動平均を計算
- **app.py**: 一時的に修正できた

## 0.0.13 (2021-01-30)

### Fix

- **app.py**: add deprecated package
- **covid.py**: renamed
- **covid19.py**: ログ出力を修正した
- **covid19.py**: メインのスクリプトも修正
- **log.py**: ロガーが動作するようにした
- **__init__.py**: log.pyをインポート
- **config.py**: 設定値にログファイルを追加
- **downloader**: ロギングを追加してみた
- **log.py**: ロギング用のモジュールを追加

## 0.0.12 (2021-01-30)

### Fix


- **loader.py**: import文を修正した
- **loader.py**: ダウンロード済みのCSVファイルをロードするモジュールを作成した
- **covid19.py**: update_v2がupdate_v1と同じになるようにした
- **covid19.py**: データ更新の関数のv1/v2を用意し、v2への移行を行った
- **covid19.py**: add deprecation message: fix_data
- **covid19.py**: ``fetch``, ``update``は廃止する予定
- **covid19.py**: 型ヒントを追加してみた
- **covid19.py**: PNG形式で画像を保存できるようになった
- **downloader.py**: fix_dataをdata.pyに移した
- **downloader.py**: ``tqdm``を追加してダウンロードの進捗を分かりやすくしてみた
- **downloader.py**: 元データの誤植を修正する関数を追加した
- **downloader.py**: ダウンロードしたデータフレームに日付を追加した
- **downloader.py**: ``argparse``を使ったコマンドラインオプションを追加した
- **downloader.py**: ``update``関数を追加した
- **downloader.py**: CSVをダウンロードする部分をモジュールとして作り直した
- **data.py**: データを加工するモジュールを追加した
- **canvas.ipynb**: 作図できるノートブックを作成した
- **version.py**: バージョンを取得する関数を作成した
- **version.py**: ``pyproject.toml``からバージョンを取得するようにした
- **__init__.py**: バージョンを取ってこれるようにした
- **config.py**: 設定値を追加（``downloaded``
- **config.py**: デフォルトの設定値を追加した
- **config.py**: 設定値のクラスを作成した


### Notebooks

- **renamed**: preprocess.ipynb -> data.ipynb
- **preprocess.ipynb**: 積算値になっているデータの差分を計算した
- **preprocess.ipynb**: データ修正の関数を移動した
- **version.ipynb**: 設定ファイルの呼び出し方について分かってきたことを整理した


### Refactor

- **version.py**: 使ってなかったモジュールを削除した
- **covid19.py**: check with flake8

## 0.0.11 (2021-01-25)

### Fix

- **version**: get version from pyproject.toml

## 0.0.10 (2020-07-13)

## 0.0.9 (2020-07-09)

## 0.0.8 (2020-06-29)

## v0.0.8 (2020-06-28)

## 0.0.7 (2020-06-22)

## v0.0.6 (2020-06-18)

## v0.0.5 (2020-06-17)

## v0.0.4 (2020-06-12)

## v0.0.3 (2020-06-08)

## v0.0.2 (2020-06-06)

## v0.0.1 (2020-06-02)
