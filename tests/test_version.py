from covid19 import version


def test_get_version():
    answer = "0.0.11"
    v = version.get_version()
    assert v == answer
