from covid19.config import Config


def test_config():
    c = Config()

    assert c.shared == "yard/"
    assert c.data == "yard/data/"
    assert c.images == "yard/images/"
    assert c.downloaded == "yard/data/downloaded.csv"
