import logging
from pathlib import Path

import config
import log
import pandas as pd
import pendulum
from icecream import ic
from tqdm import tqdm

__config = config.Config()
__logger = log.setup(__name__, __config.logfile)
__logger.debug(f"{__name__}")


def fetch_data(date: pd.Timestamp) -> pd.DataFrame:
    base = __config.url
    url = f"{base}{date.date()}.csv"
    try:
        data = pd.read_csv(url)
        data["date"] = date
    except Exception as e:
        __logger.warning(f"{e} : {url}")
        data = pd.DataFrame()
    return data


def fetch(dates: pd.DatetimeIndex) -> pd.DataFrame:
    _data = []
    for date in tqdm(dates):
        datum = fetch_data(date)
        _data.append(datum)
    data = pd.concat(_data)
    return data


def update(start: str) -> pd.DataFrame:
    end = pendulum.yesterday().to_date_string()
    __logger.info(f"Update: fetch {start} to {end}")
    dates = pd.date_range(start, end)
    data = fetch(dates)

    p = Path(__config.data)
    fname = p / "downloaded.csv"

    try:
        data.to_csv(fname, index=False)
        __logger.info(f"Saved as {fname}")
    except Exception as e:
        __logger.error(f"{e}")
    return data


if __name__ == "__main__":

    import argparse
    import sys

    def test_update():
        start = pendulum.now().subtract(days=7)
        data = update(start=start.to_date_string())
        return data

    description = "COVID19の感染者のデータを取得"
    ap = argparse.ArgumentParser(description=description)
    ap.add_argument("-t", "--test", action="store_true", help="データの範囲を限定してテスト")
    args = ap.parse_args()

    if args.test:
        test_update()
        sys.exit()
    else:
        start = "2020-03-18"
        update(start)
        sys.exit()
