from dataclasses import dataclass


@dataclass
class Config:
    url: str = "https://www.stopcovid19.jp/data/covid19japan/"
    shared: str = "yard/"
    data: str = "yard/data/"
    images: str = "yard/images/"
    start: str = "2020-03-18"
    downloaded: str = "yard/data/downloaded.csv"
    logfile: str = "yard/log/stopcovid.log"
