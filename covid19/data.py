import config
import log
import pandas as pd
from icecream import ic

__config = config.Config()
__logger = log.setup(__name__, __config.logfile)
__logger.debug(f"{__name__}")


def fix_data(data: pd.DataFrame) -> pd.DataFrame:
    # 元データの誤植を修正する
    data["name"] = data["name"].replace({"Eihime": "Ehime"})
    # 処理できない値を変換する
    rep = {"不明": 0, "-": 0}
    data["ndeaths"] = data["ndeaths"].replace(rep).astype("float64")
    data["nheavycurrentpatients"] = (
        data["nheavycurrentpatients"].replace(rep).astype("float64")
    )

    cols = len(data.columns)
    rows = len(data)
    __logger.info(f"{rows} rows / {cols} cols")
    return data


def ndiff(data: pd.DataFrame, key: str, name: str) -> pd.DataFrame:
    prefs = data["name"].unique()
    for pref in prefs:
        isT = data["name"] == pref
        _diff = data[isT][key].diff()
        data.loc[isT, name] = _diff
    return data


def add_npositive(data: pd.DataFrame) -> pd.DataFrame:
    key = "npatients"
    name = "npositive"
    data = ndiff(data, key, name)
    cols = len(data.columns)
    rows = len(data)
    __logger.info(f"{rows} rows / {cols} cols")
    return data


def add_ninspected(data: pd.DataFrame) -> pd.DataFrame:
    key = "ninspections"
    name = "ninspected"
    data = ndiff(data, key, name)
    cols = len(data.columns)
    rows = len(data)
    __logger.info(f"{rows} rows / {cols} cols")
    return data


def add_nexit(data: pd.DataFrame) -> pd.DataFrame:
    key = "nexits"
    name = "nexit"
    data = ndiff(data, key, name)
    cols = len(data.columns)
    rows = len(data)
    __logger.info(f"{rows} rows / {cols} cols")
    return data


def add_ndeath(data: pd.DataFrame) -> pd.DataFrame:
    key = "ndeaths"
    name = "ndeath"
    data = ndiff(data, key, name)
    cols = len(data.columns)
    rows = len(data)
    __logger.info(f"{rows} rows / {cols} cols")
    return data


def add_npositive7d(data: pd.DataFrame) -> pd.DataFrame:
    prefs = data["name"].unique()
    for pref in prefs:
        isT = data["name"] == pref
        _mean = data[isT]["npositive"].rolling(7).mean()
        _error = data[isT]["npositive"].rolling(7).std()
        data.loc[isT, "npositive7d"] = _mean
        data.loc[isT, "npositive7d_error"] = _error

    cols = len(data.columns)
    rows = len(data)
    __logger.info(f"{rows} rows / {cols} cols")
    return data


def add_rpositive(data: pd.DataFrame) -> pd.DataFrame:
    data["rpositive"] = data["npositive"] / data["ninspected"]
    cols = len(data.columns)
    rows = len(data)
    __logger.info(f"{rows} rows / {cols} cols")
    return data


def process(data: pd.DataFrame) -> pd.DataFrame:

    rdata = fix_data(data)
    rdata = add_npositive(rdata)
    rdata = add_ninspected(rdata)
    rdata = add_nexit(rdata)
    rdata = add_ndeath(rdata)
    rdata = add_npositive7d(rdata)
    rdata = add_rpositive(rdata)
    return rdata


if __name__ == "__main__":

    import loader

    c = config.Config()
    src = loader.load(c.downloaded)

    ic(len(src.columns))
    data = process(src)
    ic(len(data.columns))
