import config
import log
import pandas as pd
from icecream import ic

__config = config.Config()
__logger = log.setup(__name__, __config.logfile)
__logger.debug(f"{__name__}")


def load(fname):
    __logger.info(f"Load from {fname}")
    data = pd.read_csv(fname, parse_dates=["date"])
    rows = len(data)
    __logger.info(f"Loaded {rows} rows")
    return data


if __name__ == "__main__":

    c = config.Config()
    data = load(c.downloaded)
    rows = len(data)
    __logger.info(f"Loaded {rows} rows")
