import sys
from pathlib import Path

import toml

pwd = Path("covid19")
if pwd.exists():
    apath = str(pwd.resolve())
    sys.path.insert(0, apath)
    print(f"Add sys.path : {apath}")


def get_version():
    fname = Path("pyproject.toml")
    if not fname.exists():
        fname = Path("../pyproject.toml")

    print(f"Found {fname}")
    project = toml.load(fname)
    version = project.get("tool").get("commitizen").get("version")
    return version
