from pathlib import Path

import altair as alt
import pandas as pd
from deprecated import deprecated
from icecream import ic

from covid19 import config, data, downloader, loader, log, version

__config = config.Config()
__logger = log.setup(__name__, __config.logfile)
__logger.debug("-" * 50)
__logger.debug(f"{__name__}")
__version__ = version.get_version()
ic(__version__)


def bar(
    data: pd.DataFrame, y: str, title: str = "", color: str = "orange"
) -> alt.Chart:
    """
    日別の棒グラフ

    Parameters
    ----------
    data : pandas.DataFrame
        感染者数のデータ
    y : str
        縦軸に使う列の名前
    title : str
        グラフのタイトル

    Returns
    -------
    altair.Chart
        グラフ
    """

    tips = list(data.columns)
    chart = (
        alt.Chart(data)
        .mark_bar(
            color=color,
            opacity=0.7,
        )
        .encode(
            x=alt.X("date", title="日付"),
            y=y,
            tooltip=tips,
            color=alt.condition(
                alt.datum.npositive < 0, alt.value("red"), alt.value(color)
            ),
        )
        .properties(
            title=title,
        )
    )
    return chart


def npatients(data: pd.DataFrame, name: str) -> alt.Chart:
    """
    PCR検査陽性数（累積）を表示

    Parameters
    ----------
    data : pandas.DataFrame
        データフレーム
    name : str
        都道府県名（ローマ字）

    Returns
    -------
    altair.Chart
        グラフ
    """
    y = "npatients"
    title = f"PCR検査陽性数（累積） : {name}"
    color = "orange"

    q = f'name == "{name}"'
    qdata = data.query(q)
    chart = bar(qdata, y, title, color)
    return chart


def npositive(data: pd.DataFrame, name: str) -> alt.Chart:
    """
    PCR検査陽性数のグラフを表示

    Parameters
    ----------
    data : pandas.DataFrame
        データフレーム
    name : str
        都道府県名（ローマ字）

    Returns
    -------
    altair.Chart
        グラフ
    """
    y = "npositive"
    title = f"PCR検査陽性数 : {name}"
    color = "orange"

    q = f'name == "{name}"'
    qdata = data.query(q)
    chart = bar(qdata, y, title, color)
    return chart


def ndeath(data, name):
    """
    死亡数を表示

    Parameters
    ----------
    data : pandas.DataFrame
        データフレーム
    name : str
        都道府県名（ローマ字）

    Returns
    -------
    altair.Chart
        グラフ
    """
    y = "ndeath"
    title = f"死亡数 : {name}"
    color = "grey"

    q = f'name == "{name}"'
    qdata = data.query(q)
    chart = bar(qdata, y, title, color)
    return chart


def nheavy(data, name):
    """
    死亡数を表示

    Parameters
    ----------
    data : pandas.DataFrame
        データフレーム
    name : str
        都道府県名（ローマ字）

    Returns
    -------
    altair.Chart
        グラフ
    """
    y = "ndeath"
    title = f"重症患者数 : {name}"
    color = "pink"

    q = f'name == "{name}"'
    qdata = data.query(q)
    chart = bar(qdata, y, title, color)
    return chart


def npositive7d(data, name):
    y = "npositive7d"
    title = f"PCR検査陽性数（7日平均） : {name}"
    color = "green"

    q = f'name == "{name}"'
    qdata = data.query(q)

    base = bar(qdata, y, title, color)
    l = base.mark_line()
    p = base.mark_circle()

    # エラーの上限値／下限値を計算
    qdata["min"] = qdata["npositive7d"] - qdata["npositive7d_error"]
    qdata["max"] = qdata["npositive7d"] + qdata["npositive7d_error"]

    err = (
        alt.Chart(qdata).mark_errorbar(color=color).encode(x="date", y="max", y2="min")
    )
    chart = l + p + err
    return chart


def rpositive(data, name):
    """
    陽性率を表示

    Parameters
    ----------
    data : pandas.DataFrame
        データフレーム
    name : str
        都道府県名（ローマ字）

    Returns
    -------
    altair.Chart
        グラフ
    """
    y = alt.Y("rpositive", scale=alt.Scale(domain=(0, 1), clamp=True))
    title = f"陽性率 : {name}"
    color = "pink"

    q = f'name == "{name}"'
    qdata = data.query(q)
    chart = bar(qdata, y, title, color)
    return chart
